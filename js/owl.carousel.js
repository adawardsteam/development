let owl;
    owl = $('.index-wrapper .owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    responsive:{
        0:{
            items:1
        },
    }
  })
  let owl_2;
    owl_2 = $('.slider .owl-carousel').owlCarousel({
    loop:true,
    margin:40,
    nav:true,
    dots: false,
    responsive:{
        0:{
            items:1,
        },
        992:{
            items:3,
        }
    }
  })
  let owl_3;
    owl_3 = $('.investment-wrapper .owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
    }
  })
