let magicGrid = new MagicGrid({
    container: '.magic-grid',
    animate: true,
    gutter: 30,
    static: true,
    useMin: true,
    maxColumns: 2,
    
    });

    magicGrid.listen();