$(document).ready(function(){

    $(".magic-grid div").hover(
      function(){ $(this.lastElementChild).toggleClass('onhover')},
    );

    $(".btn-collapse-table").click(
      function() { 
        $('.table-section').toggleClass('max-height');
        $('.dataTables_wrapper').toggleClass('max-initial');
        $('.table-section .container').toggleClass('max-initial');
        $('.btn-collapse-table').toggleClass('bottom-0');
      })

      $('#example').DataTable({
        "paging":   false,
        "info":     false,
        "ordering": true,
        "searching": false,
          "columnDefs": [
        { "orderable": false, "targets": 6 }
      ]
      });

    });